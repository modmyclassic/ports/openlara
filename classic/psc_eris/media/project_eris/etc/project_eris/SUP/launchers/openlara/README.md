# OpenLara port for the PlayStation Classic

**OpenLara by XProger, Port by ModMyClassic**

[OpenLara Website](http://xproger.info/projects/OpenLara/)

-----

This is a port of OpenLara for the PlayStation Classic.

OpenLara is a High Definition classic Tomb Raider engine which also supports a bunch of new cool features like VR, 1st person and splitscreen co-op gameplay. 

OpenLara is still in active development and currently only fully supports Tomb Raider 1 with more game support on it's way.

## How to use

1. Drag the mod package to `USB:/project_eris/mods/` The next time you boot, it will install the game to your USB.

2. Once installed, You need to copy your Tomb Raider 1 Game Data and Audio data to the root of the port directory. (for example the PSX version:)

`DELDATA/`

`FMV/`

`PSXDATA/`

![Tomb Raider 1 Data Screenshot](https://cdn.discordapp.com/attachments/398987597592133632/575978927231860758/unknown.png)

3. Copy the Audio files to the root of the port directory. (for example the PSX version:)

(`USB:/project_eris/etc/project_eris/SUP/launchers/OpenLara/`)

`audio/`

![Tomb Raider 1 audio Screenshot](https://cdn.discordapp.com/attachments/398987597592133632/575980734679089162/unknown.png)

4. Once you have extracted your game data to the port you should end up with something like:

`USB:/project_eris/etc/project_eris/SUP/launchers/OpenLara/DELDATA/*.RAW`

`USB:/project_eris/etc/project_eris/SUP/launchers/OpenLara/PSXDATA/*.PSX`

`USB:/project_eris/etc/project_eris/SUP/launchers/OpenLara/FMV/*.FMV`

`USB:/project_eris/etc/project_eris/SUP/launchers/OpenLara/audio/*.ogg`

5. Launch the game from your PSC carousel menu and have fun!